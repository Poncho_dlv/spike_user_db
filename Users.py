#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum
from enum import IntFlag, auto

from bson.objectid import ObjectId

from spike_user_db.BaseMongoDb import BaseMongoDb


class AccountType(IntEnum):
    DISCORD = 0
    SPIKE = 1


class AccountLvl(IntFlag):
    STANDARD = auto()
    PREMIUM = auto()
    TOURNAMENT_DIRECTOR = auto()
    ADMIN = auto()
    LINK_ADMIN = auto()


class Users(BaseMongoDb):

    def add_discord_user(self, user_name: str, discord_id: int, locale: str, discriminator: str, avatar: str, token: str = None):
        self.open_database()
        collection = self.db["users"]
        user_data = {
            "user_name": user_name,
            "discord_id": discord_id,
            "lang": [locale],
            "country": locale,
            "discriminator": discriminator,
            "avatar": avatar,
            "type": AccountType.DISCORD,
            "level": AccountLvl.STANDARD
        }

        if token is not None:
            user_data['token'] = token

        ret = collection.insert_one(user_data)
        self.close_database()
        return ret

    def update_user(self, user_id: str, user_name: str = None, lang: list = None, country: str = None, avatar: str = None, twitch: str = None,
                    youtube: str = None, steam: dict = None, naf_name: str = None, naf_number: int = None, level: AccountLvl = None,
                    token: str = None):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        update_values = {}

        def build_simple_data(key, value):
            if value is not None:
                if value != "":
                    if not update_values.get("$set"):
                        update_values["$set"] = {}
                    update_values["$set"][key] = value
                else:
                    if not update_values.get("$pull"):
                        update_values["$pull"] = {}
                    update_values["$pull"][key] = value

        build_simple_data("user_name", user_name)
        build_simple_data("country", country)
        build_simple_data("lang", lang)
        build_simple_data("avatar", avatar)
        build_simple_data("twitch", twitch)
        build_simple_data("youtube", youtube)
        build_simple_data("steam", steam)
        build_simple_data("naf_name", naf_name)
        build_simple_data("naf_number", naf_number)
        build_simple_data("level", level)
        build_simple_data("token", token)

        ret = collection.update_one(query, update_values)
        self.close_database()
        return ret

    def add_bb_coach_link(self, user_id: str, coach_id: int, platform_id):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        update_values = {"$addToSet": {"blood_bowl_coaches": {"coach_id": coach_id, "platform_id": platform_id}}}
        ret = collection.update_one(query, update_values)
        self.close_database()
        return ret

    def remove_bb_coach_link(self, user_id: str, coach_id: int, platform_id):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        update_values = {"$pull": {"blood_bowl_coaches": {"coach_id": coach_id, "platform_id": platform_id}}}
        ret = collection.update_one(query, update_values)
        self.close_database()
        return ret

    def get_user_linked(self, coach_id: int, platform_id: int):
        self.open_database()
        collection = self.db["users"]
        ret = collection.find_one({"blood_bowl_coaches": {"coach_id": coach_id, "platform_id": platform_id}}, {"_id": 1})
        if ret is not None:
            return ret["_id"]
        self.close_database()
        return None

    def remove_twitch_link(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        new_value = {"$unset": {"twitch": 1}}
        collection.update_many(query, new_value)
        self.close_database()

    def remove_steam_link(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        new_value = {"$unset": {"steam": 1}}
        collection.update_many(query, new_value)
        self.close_database()

    def delete_user(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        ret = collection.delete_one(query)
        self.close_database()
        return ret

    def get_user(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        user = collection.find_one(query)
        self.close_database()
        return user

    def get_users(self, user_ids: list):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": {"$in": user_ids}}
        users = list(collection.find(query))
        self.close_database()
        return users

    def get_discord_user(self, discord_id: int):
        self.open_database()
        collection = self.db["users"]
        query = {"discord_id": discord_id}
        user = collection.find_one(query)
        self.close_database()
        return user

    def get_discord_users(self, discord_ids: list):
        self.open_database()
        collection = self.db["users"]
        query = {"discord_id": {"$in": discord_ids}}
        users = list(collection.find(query))
        self.close_database()
        return users

    def user_exist(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        user = collection.find_one(query, {"_id": 1})
        if user is not None:
            self.close_database()
            return True
        self.close_database()
        return False

    def get_discord_token(self, user_id: str):
        self.open_database()
        collection = self.db["users"]
        query = {"_id": ObjectId(user_id)}
        token = collection.find_one(query, {"_id": 0, "token": 1})
        self.close_database()
        return token

    def get_users_like(self, search: str, limit: int = 9):
        self.open_database()
        collection = self.db["users"]
        query = {"user_name": {'$regex': "{}".format(search), "$options": "i"}}
        users = list(collection.find(query, {"_id": 1, "discord_id": 1, "user_name": 1}).limit(limit))
        self.close_database()
        return users
